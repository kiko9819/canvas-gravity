const canvas = document.querySelector('canvas');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let context = canvas.getContext('2d');

window.addEventListener('resize', function() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  init();
});

let colors = [
  "#95EEF8",
  "#DEDEDE",
  "#EF6527",
  "#C0E941",
  "#6A68DB",
];
let balls = [];
let friction = 0.9;
let gravity = 1;

function randomNumberInRange(from, to) {
  return Math.floor(Math.random() * (to - from + 1) + from);
};

function Ball(x, y, dx, dy, radius) {
  this.x = x;
  this.y = y;
  this.dy = dy;
  this.dx = dx;
  this.radius = radius;
  this.color = colors[Math.floor(Math.random() * colors.length)];

  this.draw = function() {
    context.beginPath();
    context.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    context.fillStyle = this.color;
    context.fill();
    context.closePath();
  }
  this.update = function() {
    if (this.y + this.radius + this.dy > canvas.height && this.radius >0) {
      this.dy = -this.dy * friction;
      this.radius--;
    } else {
      this.dy += gravity;
    }
    if (this.x + this.radius + this.dx > canvas.width ||
      this.x - this.radius <= 0) {
      this.dx = -this.dx;
    }
    this.y += this.dy;
    this.x += this.dx;
    this.draw();
  }
}
let ball;

function init() {
  balls = [];
  for (var i = 0; i < 1000; i++) {
    let radius = randomNumberInRange(8, 30);
    let x = randomNumberInRange(radius, canvas.width - radius);
    let y = randomNumberInRange(radius, canvas.height - radius);
    let dx = randomNumberInRange(-2, 2);
    let dy = randomNumberInRange(-2, 2);
    balls.push(new Ball(x, y, dx, dy, radius));
  }
}

function animate() {
  requestAnimationFrame(animate);
  context.clearRect(0, 0, canvas.width, canvas.height);
  for (var i = 0; i < balls.length; i++) {
    balls[i].update();
  }
}
init();
animate();
